# 
Interactive tool for projecting KPIs over a 2 year horizon given install and retention curves

## Setup

Run the following in the project directory:

```
install.packages("packrat")
packrat::restore()

# Start the application
shiny::runApp()

```